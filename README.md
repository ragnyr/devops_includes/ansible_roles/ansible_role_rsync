# Ansible Role: Rsync
Ansible роль устанавливает и настраивает Rsync сервер на Ubuntu серверы.

Для настройки необходимо заполнить переменную "rsync_directories".

Пример:
```
rsync_directories:
  - name: dir1
    use_chroot: false
    path: /path/to/dir1
    hosts_allow: 192.168.1.1
    hosts_deny: *
    list: true
    uid: root
    gid: root
    read_only: false
    auth_user: user1
    password: '12345'
    secrets_file: /etc/rsyncd.scrt
```
